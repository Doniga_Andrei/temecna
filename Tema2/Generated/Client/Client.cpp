
#include <iostream>
#include <DataService.grpc.pb.h>
#include <string>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

bool anBisect(std::string an)
{

	int anNumar = std::stoi(an);

	if ((anNumar % 4 == 0) && (anNumar % 100 != 0))
		return true;

	if (anNumar % 400 == 0)
		return true;

	return false;


}


int main()
{
	grpc_init();
	ClientContext context;
	auto data_stub = DataService::NewStub(grpc::CreateChannel("localhost:8888", grpc::InsecureChannelCredentials()));
	DataRequest dataRequest;
	std::string data;

	bool valid = true;

	std::cout << "Introduceti data nasterii dumneavoastra: ";
	std::cin >> data;

	int numarLuna = stoi(data.substr(0, 2));


	if (data.substr(0, 2) == "02")
	{
		if (anBisect(data.substr(6, 4)))
		{
			if (data.substr(0, 2) < "01" || data.substr(0, 2) > "12"
				|| data.substr(3, 2) < "01" || data.substr(3, 2) > "29")
			{
				std::cout << "Eroare Input \n";
				valid = false;
			}
		}
		else
		{
			if (data.substr(0, 2) < "01" || data.substr(0, 2) > "12"
				|| data.substr(3, 2) < "01" || data.substr(3, 2) > "28")
			{
				std::cout << "Eroare Input \n";
				valid = false;
			}
		}
	}
	else
		if (numarLuna % 2 != 0)
		{
			if (data.substr(0, 2) < "01" || data.substr(0, 2) > "12"
				|| data.substr(3, 2) < "01" || data.substr(3, 2) > "31")
			{
				std::cout << "Eroare Input \n";
				valid = false;
			}
		}
		else
		{
			if (numarLuna % 2 == 0)
			{
				if (data.substr(0, 2) < "01" || data.substr(0, 2) > "12"
					|| data.substr(3, 2) < "01" || data.substr(3, 2) > "30")
				{
					std::cout << "Eroare Input \n";
					valid = false;
				}
			}

		}


	if (valid)
	{
		dataRequest.set_data(data);

		DataReply dataReply;

		auto status = data_stub->GetData(&context, dataRequest, &dataReply);

		if (status.ok())
		{
			std::cout << dataReply.reply() << std::endl;
		}
		else
		{
			std::cout << "Error 101. \n";
		}

	}

	return 0;

}
