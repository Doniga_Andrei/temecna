#include "DataServiceImplement.h"
#include <fstream>
#include <vector>

bool anBisect(std::string an)
{

	int anNumar = std::stoi(an);

	if ((anNumar % 4 == 0) && (anNumar % 100 != 0))
		return true;

	if (anNumar % 400 == 0)
		return true;

	return false;


}

::grpc::Status DataServiceImplement::GetData(::grpc::ServerContext* context, const::DataRequest* request, ::DataReply* reply)
{
	struct Data {
		std::string lunaInceput;
		std::string ziInceput;
		std::string lunaSfarsit;
		std::string ziSfarsit;
		std::string zodie;
	};

	std::ifstream f("Zodiac.txt");

	std::string dataPersoana = request->data();
	std::string lunaPersoana = dataPersoana.substr(0, 2);
	std::string ziPersoana = dataPersoana.substr(3, 2);
	std::string anPersoana = dataPersoana.substr(6, 4);

	std::vector<Data> data;
	Data auxData;
	std::string line;


	while (getline(f, line))
	{

		auxData.lunaInceput = line.substr(0, 2);
		auxData.ziInceput = line.substr(3, 2);
		getline(f, line);
		auxData.lunaSfarsit = line.substr(0, 2);
		auxData.ziSfarsit = line.substr(3, 2);
		getline(f, line);
		auxData.zodie = line;

		data.push_back(auxData);

	}

	for (auto dataObj : data)
	{

		if (anBisect(anPersoana))
		{
			if (lunaPersoana == "02" && ziPersoana == "29")
			{
				reply->set_reply("Pesti");
				return ::grpc::Status::OK;
			}
		}


		if (dataObj.lunaInceput == "12" && dataObj.lunaSfarsit == "01")
		{
			if (dataObj.ziInceput <= ziPersoana)
			{
				reply->set_reply(dataObj.zodie);

				return ::grpc::Status::OK;
			}
		}
		else
		{
			if (dataObj.lunaInceput == lunaPersoana && dataObj.lunaSfarsit > lunaPersoana)
			{
				if (dataObj.ziInceput <= ziPersoana)
				{
					reply->set_reply(dataObj.zodie);
					return ::grpc::Status::OK;
				}

			}
			else
			{
				if (dataObj.lunaInceput < lunaPersoana && dataObj.lunaSfarsit == lunaPersoana)
				{
					if (dataObj.ziSfarsit >= ziPersoana)
					{
						reply->set_reply(dataObj.zodie);
						return ::grpc::Status::OK;
					}
				}
			}
		}
	}



	//return ::grpc::Status::OK;
}
