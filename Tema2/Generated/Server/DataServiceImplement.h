#pragma once

#include <DataService.grpc.pb.h>

class DataServiceImplement final : public DataService::Service
{

public:
	DataServiceImplement() {};

	
	::grpc::Status GetData(::grpc::ServerContext* context, const ::DataRequest* request, ::DataReply* reply) override;


};

